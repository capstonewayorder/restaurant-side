//Author : Rohan Patel

import React, {useEffect, useRef, useState} from "react";
import {connect} from "react-redux";
import Styles from "./MenuAddItemsStyle.css";
import {useToasts} from 'react-toast-notifications';
import {confirmAlert} from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import axios from "axios";
import ReactStars from 'react-stars';
import {
    CustomInput,
    Row,
    Col,
    InputGroup,
    Label,
    Form,
    FormText,
    Input,
    FormGroup,
    InputGroupAddon,
    InputGroupText
} from "reactstrap";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPen, faTrash, faTimes} from "@fortawesome/free-solid-svg-icons";
import {baseURL} from "../../../baseURL";
import {addCategory, deleteCategory, updateCategory} from "../../../Redux/RestaurantUser/Actions/restaurantUserAction";
import {storage} from "../../../firebase/firebase";
import {v4 as uuidv4} from 'uuid';

const MenuCategory = props => {
    let d = {};
    const defaultDishUrl = "https://png.pngtree.com/png-vector/20190318/ourlarge/pngtree-junk-food-seamless-pattern-doodle-drawing-style-line-art-hand-png-image_858333.jpg";
    let originalDishUrl = "";
    try {
        d = props.location.state.dish;
        originalDishUrl = d.image;
        console.log(d);
    } catch (e) {
        try {
            d = {id: -1, name: "", description: "", price: 0, tax: 0, categoryId: props.categories[0].id};
        } catch (e) {
            d = {id: -1, name: "", description: "", price: 0, tax: 0, categoryId: 0};
        }
    }


    const [name, setName] = useState(d.name);
    const [description, setDescription] = useState(d.description);
    const [price, setPrice] = useState(d.price);
    const [tax, setTax] = useState(d.tax);
    const [category, setCategory] = useState(d.categoryId);
    const {addToast} = useToasts()

    const allInputs = {imgUrl: originalDishUrl === "" ? defaultDishUrl : originalDishUrl}
    const [imageAsFile, setImageAsFile] = useState('')
    const [imageAsUrl, setImageAsUrl] = useState(allInputs)

    const handleImageAsFile = (e) => {
        const image = e.target.files[0]
        setImageAsFile(imageFile => (image))
    }


    const handleFireBaseUpload = e => {

        e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (imageAsFile === '') {
            console.error(`not an image, the image file is a ${typeof (imageAsFile)}`)
        } else {
            const fileName = uuidv4();
            const extension = "." + imageAsFile.name.split(".")[1];
            const uploadTask = storage.ref('/images/' + props.restaurantId + "/" + fileName + extension).put(imageAsFile)
            //initiates the firebase side uploading
            uploadTask.on('state_changed',
                (snapShot) => {
                    //takes a snap shot of the process as it is happening
                    console.log(snapShot)
                }, (err) => {
                    //catches the errors
                    console.log(err)
                }, () => {
                    // gets the functions from storage refences the image storage in firebase by the children
                    // gets the download url then sets the image from firebase as the value for the imgUrl key:
                    storage.ref('images/' + props.restaurantId).child(fileName + extension).getDownloadURL()
                        .then(fireBaseUrl => {
                            console.log("FIREBASE URL IS : ")
                            console.log(fireBaseUrl);
                            setImageAsUrl(prevObject => ({...prevObject, imgUrl: fireBaseUrl}))
                        })
                })
        }
    }

    const onPriceChangeHandler = value => {
        if (!isNaN(value)) {
            setPrice(value);
        }
    };

    const onTaxChangeHandler = value => {
        if (!isNaN(value)) {
            setTax(value);
        }
    }

    const onAddItemButtonHandler = () => {
        if (category == 0) {
            alert("Create a new category to add items");
            return;
        }
        if (name.trim() === "") {
            alert("Empty name");
            return;
        }

        const dishObj = {
            name: name,
            price: price,
            description: description,
            reviews: 20,
            tax: tax,
            image: imageAsUrl.imgUrl
        };
        axios.put(baseURL + "/restaurant/" + props.restaurantId + "/menu/item?categoryId=" + category,
            dishObj).then(res => {
            if (res.data.code == 0) {
                addToast(res.data.message, {
                    appearance: 'success',
                    autoDismiss: true
                });
            }
        }).catch(err => {
            console.log(err);

        })
    };

    const onUpdateClickHandler = () => {
        const url = baseURL + "/restaurant/" + props.restaurantId + "/menu/item?fromCategoryId="
            + d.categoryId + "&categoryId=" + category;

        const dishObj = {
            id: d.id,
            name: name,
            price: price,
            description: description,
            tax: tax,
            image: imageAsUrl.imgUrl
        };

        axios.post(url, dishObj).then(res => {
            if (res.data.code == 0) {
                addToast(res.data.message, {
                    appearance: 'success',
                    autoDismiss: true
                });
            }
        }).catch(err => {
            console.log(err);
        })

    };
    return (
        <div>


            <Row>
                <Col lg={6} md={6}>
                    <Row>
                        <Col>
                            <div className="mt-3">
                                <span>Item name</span>
                                <Input type="text" value={name} className={"mt-2"}
                                       onChange={(e) => setName(e.target.value)}/>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>

                            <div className={"mt-3"}>
                                <span>Price</span>
                                <InputGroup className={"mt-2"}>
                                    <InputGroupAddon addonType={"prepend"}>
                                        <InputGroupText>$</InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" value={price}
                                           onChange={(e) => onPriceChangeHandler(e.target.value)}/><br/>
                                </InputGroup>
                            </div>
                        </Col>
                        <Col>
                            <div className={"mt-3"}>
                                <span>Tax</span>
                                <InputGroup className={"mt-2"}>
                                    <InputGroupAddon addonType={"prepend"}>
                                        <InputGroupText>%</InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" value={tax}
                                           onChange={(e) => onTaxChangeHandler(e.target.value)}/>
                                </InputGroup>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className={"mt-3"}>
                                <span>Category</span>
                                <Input type="select" className={"mt-2"} value={category}
                                       onChange={(e) => setCategory(e.target.value)}>
                                    {props.categories.map(c => {
                                        return <option value={c.id} key={c.id}>{c.name}</option>
                                    })}
                                </Input>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col lg={6} md={6}>
                    <Row>
                        <Col>
                            <div className="mt-3">
                                <span>Item description</span>
                                <Input type="textarea" className={"mt-2"}
                                       value={description}
                                       onChange={(e) => setDescription(e.target.value)}/>
                            </div>

                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className="mt-3">
                                <Form onSubmit={handleFireBaseUpload}>
                                    <FormGroup>
                                        <Label for="exampleFile">File</Label>
                                        <Input type="file" name="file" id="exampleFile" accept=".jpeg,.jpg,.png"
                                               type="file"
                                               onChange={handleImageAsFile}/>
                                        <FormText color="muted">
                                            Select JPEG | PNG image and see the preview of your menu item
                                        </FormText>
                                        <button className={"secondaryButton"}>Confirm Image</button>
                                    </FormGroup>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className={"preview-container shadow bg-white"}>
                                <Row>
                                    <Col lg={4} md={5} sm={5} xs={5}>
                                        <img
                                            src={imageAsUrl.imgUrl}
                                            className={"preview-image"}/>
                                    </Col>
                                    <Col lg={8} md={7} sm={7} xs={7} className="pt-2">
                                        <span className={"preview-name"}>{name}</span>
                                        <p className={"preview-description"}>{description}</p>
                                        <ReactStars
                                            value={4.5}
                                            size={24}>4.5 Stars</ReactStars>
                                        <span>Price : ${price}</span>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>

                </Col>
            </Row>
            <Row>
                <Col>
                    <div className={"mt-3"}>
                        {d.id === -1 &&
                        <button className="secondaryButton" onClick={() => onAddItemButtonHandler()}>Add item</button>}
                        {d.id !== -1 &&
                        <button className="ternaryButton" onClick={() => onUpdateClickHandler()}>Update Item</button>}
                    </div>
                </Col>


            </Row>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        categories: state.restaurantUser.restaurant.menu.categories,
        restaurantId: state.restaurantUser.restaurant.id
    };
};

const mapActionsToProps = {
    addNewCategory: addCategory,
    updateCategory: updateCategory,
    deleteCategory: deleteCategory
};
export default connect(mapStateToProps, mapActionsToProps)(MenuCategory);
