//Vishvakumar Mavani

import React, {useEffect, useState} from "react";
import {Row, Col, Input, Table} from "reactstrap";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {SyncLoader} from "react-spinners";
import {
    faTrash,
    faDownload,
    faQrcode
} from "@fortawesome/free-solid-svg-icons";
import Styles from "./AddTableStyle.css";
import {useToasts} from 'react-toast-notifications';
import axios from "axios";
import {baseURL} from "../../../baseURL";
import {addTable, deleteTable} from "../../../Redux/RestaurantUser/Actions/restaurantUserAction";

const AddTable = props => {

    const baseQrUrl = "https://api.qrserver.com/v1/create-qr-code/?size=800x800&data=";
    const [tableNumber, setTableNumber] = useState(1); // table number for input field
    const [tableCapacity, setTableCapacity] = useState(1) //table capactiy input field
    const [qrCodeTableNumber, setQrCodeTableNumber] = useState(0); // table number for displayed qr code
    const [qrUrl, setQrUrl] = useState("");
    const [loading, setLoading] = useState(false);
    const {addToast} = useToasts()


    const onAddButtonClickHandler = () => {

        const tableObj = {
            number: tableNumber,
            capacity: tableCapacity
        }
        axios.put(baseURL + "/restaurant/" + props.restaurantId + "/table", tableObj).then(res => {
            if (res.data.code == 0) {
                addToast(res.data.message, {
                    appearance: 'success',
                    autoDismiss: true
                })
                props.onAddTable(res.data.object);
            }
            if (res.data.code == 1) {
                addToast(res.data.message, {
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        }).catch(err => {
            console.log(err);
        })
    };


    const onDeleteButtonClickHandler = tid => {
        axios.delete(baseURL + "/restaurant/" + props.restaurantId + "/table/" + tid).then(res => {
            if (res.data.code == 0) {
                addToast(res.data.message, {
                    appearance: 'info',
                    autoDismiss: true
                })
                props.onDeleteTable(tid);
            }
        }).catch(err => {
            console.error(err);
        })
    };

    const onGenerateButtonClickHandler = (tid, tnum) => {
        const qrText = {
            id: props.restaurantId,
            table: tid
        }

        const url = baseQrUrl + JSON.stringify(qrText);
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
            setQrCodeTableNumber(tnum);
            setQrUrl(url);
        }, 1200);

    };

    return (
        <div>
            <Row>
                {/*Col for table number input and display*/}
                <Col lg={7} md={7} sm={12} xs={12}>
                    {/*Input row for table number*/}
                    <Row>
                        <Col lg={4} md={4}>
                            <Input value={tableNumber} className="mt-2" type="number" min={1}
                                   onChange={(e) => setTableNumber(e.target.value)} placeholder={"Table No."}/>
                            <Input value={tableCapacity} className="mt-2" type="number" min={1}
                                   onChange={(e) => setTableCapacity(e.target.value)} placeholder={"Capacity"}/>

                        </Col>
                        <Col>
                            <button className={"primaryButton mt-2"} onClick={() => onAddButtonClickHandler()}>Add
                            </button>
                        </Col>
                    </Row>

                    {/*Row for displaying added tables*/}
                    <Row>
                        <Col>
                            <Table dark className={"mt-5"}>
                                <thead>
                                <tr>
                                    <th>Table number</th>
                                    <th>Capicity</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    props.tables.map(t => {
                                        return (
                                            <tr>
                                                <td>{t.number}</td>
                                                <td>{t.capacity}</td>
                                                <td>
                                                    <FontAwesomeIcon icon={faTrash} className="iconHover"
                                                                     onClick={() => onDeleteButtonClickHandler(t.id)}/>
                                                    <FontAwesomeIcon icon={faQrcode} className="ml-3 iconHover"
                                                                     onClick={() => onGenerateButtonClickHandler(t.id, t.number)}/>
                                                </td>
                                            </tr>
                                        );
                                    })
                                }
                                </tbody>
                            </Table>
                        </Col></Row>
                </Col>


                {/*Col for displaying table's QR code*/}
                <Col>
                    <div style={{
                        display: "grid",
                        justifyContent: "center",
                        alignItems: 'center',
                        flexDirection: "column"
                    }}>
                        {loading &&
                        <div>
                            <SyncLoader size={20} color={"#0033CC"}/><br/>
                            <span class="font-weight-bold mt-3">Generating QR code</span>
                        </div>}
                        {!loading && <img height={300} src={qrUrl}/>}
                        <br/><br/>
                        {qrUrl !== "" &&
                        <div style={{display: "grid", justifyContent: "center", alignItems: "center"}}>
                            <span className={"font-weight-bold mt-3"}>Table number : {qrCodeTableNumber}</span><br/>
                            <a href={qrUrl} download target={"_blank"} class="btn btn-success mt-4">Download
                                <FontAwesomeIcon icon={faDownload} className="ml-2"/>
                            </a>
                        </div>}
                    </div>

                </Col>
            </Row>
        </div>
    );
}


const mapStateToProps = state => {
    return {
        tables: state.restaurantUser.restaurant.tables,
        restaurantId: state.restaurantUser.restaurant.id
    }
}

const mapActionToProps = {
    onAddTable: addTable,
    onDeleteTable: deleteTable
};
export default connect(mapStateToProps, mapActionToProps)(AddTable);