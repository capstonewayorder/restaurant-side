//Author : Vishvakumar Mavani
import React, {useState} from "react";
import {Row, Col} from "reactstrap";
import styles from "./RestaurantDetailsStyle.css";
import {storage} from "../../../firebase/firebase";
import {
    Dropdown,
    Form,
    Input
} from "reactstrap";
import {v4 as uuidv4} from 'uuid';
import {connect} from "react-redux";
import axios from "axios";
import {baseURL} from "../../../baseURL";

import {useToasts} from "react-toast-notifications";
import {addRestaurant} from "../../../Redux/RestaurantUser/Actions/restaurantUserAction";
import BusinessHours from "../../../Components/BusinessHours/BusinessHours";

const RestaurantDetails = props => {
    const {addToast} = useToasts();
    const allInputs = {imgUrl: props.restaurantUser.restaurant.logo}
    const allbgInputs = {bgimgUrl: props.restaurantUser.restaurant.bgImg}

    const [imageAsFile, setImageAsFile] = useState('')
    const [bgimageAsFile, setbgImageAsFile] = useState('')

    const [imageAsUrl, setImageAsUrl] = useState(allInputs)
    const [bgimageAsUrl, setbgImageAsUrl] = useState(allbgInputs)

    console.log(props.restaurantUser);
    // Image
    const handleImageAsFile = (e) => {
        const image = e.target.files[0]
        setImageAsFile(imageFile => (image))
        // document.getElementById("newavatar").src = document.getElementById("logoFile").value;
    }

    // Background Image
    const handlebgImageAsFile = (e) => {
        const bgimage = e.target.files[0]
        setbgImageAsFile(imageFile => (bgimage))
    }

    // Image
    const handleFireBaseUpload = e => {

        e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if (imageAsFile === '') {
            console.error(`not an image, the image file is a ${typeof (imageAsFile)}`)
        } else {
            const fileName = uuidv4();
            const extension = "." + imageAsFile.name.split(".")[1];
            const uploadTask = storage.ref('/logos/' + props.restaurantId + "/" + fileName + extension).put(imageAsFile)
            //initiates the firebase side uploading
            uploadTask.on('state_changed',
                (snapShot) => {
                    //takes a snap shot of the process as it is happening
                    console.log(snapShot)
                }, (err) => {
                    //catches the errors
                    console.log(err)
                }, () => {
                    // gets the functions from storage refences the image storage in firebase by the children
                    // gets the download url then sets the image from firebase as the value for the imgUrl key:
                    storage.ref('logos/' + props.restaurantId).child(fileName + extension).getDownloadURL()
                        .then(fireBaseUrl => {
                            setImageAsUrl(prevObject => ({...prevObject, imgUrl: fireBaseUrl}))
                            console.log("FIREBASE URL IS : " + fireBaseUrl)
                            let cpyResUserObj = props.restaurantUser;
                            cpyResUserObj.restaurant.logo = fireBaseUrl;
                            props.onRestaurantUpdate(cpyResUserObj);
                            uploadImage(fireBaseUrl)
                        })
                })
        }
    }

    //Background Image

    const handleFireBaseUploadbgimage = e => {
        e.preventDefault()
        console.log("start to upload Bg Image")
        //async magic goes here for bg image ....
        if (bgimageAsFile == '') {
            console.error(`not an image, the image file is a ${typeof (bgimageAsFile)}`)
        } else {
            const bgfileName = uuidv4();
            const bgextension = "." + bgimageAsFile.name.split(".")[1];
            const bguploadTask = storage.ref("/bgimage/" + props.restaurantId + "/" + bgfileName + bgextension).put(bgimageAsFile)

            bguploadTask.on('state_changed',
                (bgsnapShot) => {
                    console.log(bgsnapShot)
                }, (err) => {
                    console.log(err)
                }, () => {
                    storage.ref("bgimage/" + props.restaurantId).child(bgfileName + bgextension).getDownloadURL()
                        .then(bgfireBaseUrl => {
                            setbgImageAsUrl(prevObject => ({...prevObject, bgimgUrl: bgfireBaseUrl}))
                            console.log("FireBase URL for Background image is" + bgfireBaseUrl)
                            let cpyResUserObj = props.restaurantUser;
                            cpyResUserObj.restaurant.bgImg = bgfireBaseUrl;
                            props.onRestaurantUpdate(cpyResUserObj);
                            uploadbgImage(bgfireBaseUrl)
                        })
                }
            )
        }
    }

    const uploadImage = url => {
        console.log("URL IS : " + url);
        axios.put(baseURL + "/restaurant/" + props.restaurantId + "/logo?logoUrl=" + encodeURI(url))
            .then(res => {
                if (res.data.code == 0) {
                    console.log(res.data);
                    addToast(res.data.message, {
                        appearance: 'success',
                        autoDismiss: true
                    });
                } else {
                    addToast(res.data.message, {
                        appearance: 'NOT SUCCESS',
                        autoDismiss: true
                    });
                }
            }).catch(err => {

            console.log(err);
        })
    };

    const uploadbgImage = bgurl => {
        console.log("BG URL is :" + bgurl);

        axios.put(baseURL + "/restaurant/" + props.restaurantId + "/bgImg?bgImgUrl=" + encodeURI(bgurl))
            .then(res => {
                if (res.data.code == 0) {
                    console.log(res.data);

                }
            }).catch(err => {

            console.log(err);
            console.log(props.restaurantId)
        })
    }


    return (
        <>


            <div class="card-deck mb-3 text-center">
                <div class="card mb-2 box-shadow">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Upload Restaurant Image</h4>
                    </div>
                    <div className="card-body">
                        {/* this is forclass="rounded-circle z-depth-1-half avatar-pic" Background Image*/}
                        <div className="imageContainer">

                            <img src={imageAsUrl.imgUrl} className={"restaurant-bgimage"}/>
                            <Form onSubmit={handleFireBaseUpload} id="formtoupload">
                                <Input type="file" className="inputUpload" name="file" accept=".jpeg,.jpg,.png"
                                       type="file"
                                       onChange={handleImageAsFile}/>
                                <button className="primaryButton">Submit</button>
                            </Form>
                        </div>
                    </div>
                </div>
                <div className="card mb-2 box-shadow">
                    <div className="card-header">
                        <h4 className="my-0 font-weight-normal">Upload Background Image</h4>
                    </div>
                    <div className="card-body">
                        {/* this is forclass="rounded-circle z-depth-1-half avatar-pic" Background Image*/}

                        {/* this is forclass="rounded-circle z-depth-1-half avatar-pic" Background Image*/}
                        <div className="imageContainer">
                            <img src={bgimageAsUrl.bgimgUrl} className={"restaurant-bgimage"}/>
                            <Form onSubmit={handleFireBaseUploadbgimage} id="formtouploadbgimage">
                                <Input type="file" className="inputUpload" name="file"
                                       accept=".jpeg,.jpg,.png"
                                       type="file"
                                       onChange={handlebgImageAsFile}/>
                                <button className="primaryButton">Submit</button>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
            <BusinessHours/>
        </>
    );
};


const mapStateToProps = state => {
    return {
        restaurantUser: state.restaurantUser,
        restaurantId: state.restaurantUser.restaurant.id
    };
};

const mapActionsToProps = {
    onRestaurantUpdate: addRestaurant
};

export default connect(mapStateToProps, mapActionsToProps)(RestaurantDetails);