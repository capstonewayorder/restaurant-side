//Author : Rohan Patel
import {
    ADD_CATEGORY,
    ADD_RESTAURANT,
    ADD_TABLE,
    DELETE_CATEGORY, DELETE_TABLE,
    UPDATE_CATEGORY, UPDATE_DINEIN, UPDATE_TAKEOUT,
    VIEW_RESTAURANT_DATA
} from "./actionTypes";

export const viewRestaurantData = () => {
    return {
        type: VIEW_RESTAURANT_DATA,
        payload: {name: "PAYLOAD"}
    };
};

export const addRestaurant = restaurantUserObj => {
    return {
        type: ADD_RESTAURANT,
        payload: restaurantUserObj
    };
};

export const addCategory = newCategoryObj => {
    return {
        type: ADD_CATEGORY,
        payload: newCategoryObj
    }
};

export const updateCategory = updatedCategoryObj => {
    return {
        type: UPDATE_CATEGORY,
        payload: updatedCategoryObj
    }
}

export const deleteCategory = categoryId => {
    return {
        type: DELETE_CATEGORY,
        payload: categoryId
    }
}

export const addTable = newTableObj => {
    return {
        type: ADD_TABLE,
        payload: newTableObj
    }
}

export const deleteTable = tid => {
    return {
        type: DELETE_TABLE,
        payload: tid
    }
}

export const updateDineIn = val => {
    return {
        type: UPDATE_DINEIN,
        payload: val
    }
}

export const updateTakeOut = val => {
    return {
        type: UPDATE_TAKEOUT,
        payload: val
    }
}