//Author : Rohan Patel
import React from "react";
import styles from "./RestaurantMainPageStyle.css"
import {Row, Col} from "reactstrap";

const RestaurantMainPage = props => {
    return (
        <div className={"main-page container"}>
            <Row>
                <Col className={"main-left-content"} xs={12} md={6} lg={6}>
                    <span className={"intro-line"}>More customer for your</span>
                    <span className={"restaurant-name"}>Restaurant</span>
                    <p className={"market-line"}>Take your business to a new level with WayOrder</p>
                    <button className={"grow-with-us-button"} onClick={() => props.history.push("/register")}>
                        Grow with us
                    </button>
                </Col>
                <Col className={"main-right-content"}>
                    <img src={"undraw_data_xmfy.svg"} height={300} className={"metrics-image"}/>
                </Col>
            </Row>
        </div>
    )
};

export default RestaurantMainPage