//Author : Rohan Patel
import React, {useState} from "react";
import RestaurantPageNavBar from "../NavBar/RestaurantPageNavBar";
import RestaurantMainPage from "../MainScreen/RestaurntMainPage";
import RestaurantIntoPage from "../Intro/Intro"
import RestaurantDinePage from "../Services/DineIn/DineInService";
import RestaurantQRCodePage from "../Services/QRCode/QRCodeService"
import RestaurantReservationService from "../Services/Reservation/ReservationService"
import RestaurantWhyWayOrderPage from "../PointWhyWayOrder/Points"
import RestayrantContactUsPage from "../ContactUs/ContactUs"
import RestaurantLoader from "../Loader/Loader"
import style from "./RestaurantIndexPageStyle.css"

const RestaurantIndexPage = props => {
    //
    const [open, setOpen] = useState(false);
    console.log(props.history);
    return (

        <div className={"test"}>
            <div className={"circle-blue"}/>
            {/* <RestaurantLoader /> */}
            <RestaurantPageNavBar displayLogin={true} history={props.history}/>
            <RestaurantMainPage history={props.history}/>

            <RestaurantDinePage/>
            <RestaurantQRCodePage/>
            <RestaurantReservationService/>
            <RestaurantWhyWayOrderPage/>
            {/*<RestayrantContactUsPage/>*/}
        </div>

    )
};

export default RestaurantIndexPage;