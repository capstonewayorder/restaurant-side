import React, {useState} from "react"
import {Row, Col} from "reactstrap"
import Fade from "react-reveal"
import "./ContactUs.css";
import {db} from "../../firebase/firebase1"

function sendMail() {
    var link = "mailto:patel943@sheridancollege.ca"


        + "&subject=" + encodeURIComponent("This is my subject")
        + "&body=" + encodeURIComponent(document.getElementById('message').value)
    ;

    window.location.href = link;
}

export default function ContactUs() {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();

        db.collection('contacts').add({
            name: name,
            email: email,
            message: message,
        })
            .then(() => {
                alert("Message has been Submitted Successfully")
                setName("")
                setEmail("")
                setMessage("")
            })
            .catch((error) => {
                alert(error.message);
            })
    }

    return (
        <>
            <h1 className={"heading"}>Keep In touch</h1>
            <div className={"containers"}>
                <Row>
                    <Col className={"contact-us-left-col"}>
                        <img alt="Food Ordering" className={"img-contact-us"}
                             src={require("../../assests/images/mailImage.svg")}></img>
                    </Col>
                    <Col className={"contact-us-right-col"} class="wrap-input100 validate-input">

                        <form class="contact100-form validate-form" onSubmit={handleSubmit}>

                            <div class="wrap-input100 validate-input" data-validate="Name is required">
                                <input class="input100" type="text" placeholder="Name" value={name} required
                                       onChange={(e) => setName(e.target.value)}/>
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
                            </div>

                            <div class="wrap-input100 validate-input"
                                 data-validate="Valid email is required: ex@abc.xyz">
                                <input class="input100" type="text" name="email" placeholder="Email" required
                                       value={email} required onChange={(e) => setEmail(e.target.value)}/>
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                            </div>

                            <div class="wrap-input100 validate-input" data-validate="Message is required">
                                <textarea class="input100" id="message" name="message" placeholder="Message" required
                                          value={message} required
                                          onChange={(e) => setMessage(e.target.value)}></textarea>
                                <span class="focus-input100"></span>
                            </div>

                            <div class="container-contact100-form-btn">
                                <button className="primaryButton1" onClick="sendMail(); return false">
                                    Send
                                </button>
                            </div>
                        </form>

                    </Col>
                </Row>
            </div>
        </>
    );
}
  