//Author : Jay Patel
import React from "react"
import Fade from "react-reveal"
import {Row, Col} from "reactstrap";
import "./ReservationService.css";


export default function ReservationService() {
    return (
        <div className={"container"} style={{marginTop: '100px'}}>
            <Row>
                <Col className={"line-left-col"}>
                    <img alt="Food Ordering" src={require("../../../assests/images/Line.jpg")} className={"line-img"}/>

                </Col>
                <Col xs={12} md={6} lg={6} className={"line-right-col"}>
                    <h1 className="intro-heading">Prepaid Food and reserved <br/> table</h1>
                    <p className="intro-text-subtitle">Customer can Reserve a table and also
                        order mouth watering dishes right from their comfort of home</p>
                </Col>
            </Row>
        </div>
    );
}
  