//Author : Jay Patel
import React from "react"
import {Row, Col} from "reactstrap";
import Fade from "react-reveal"
import styles from "./DineInService.css";


const DineInService = props => {
    return (
        <div className={"container"}>
            <Row>
                <Col className={"dine-in-left-col"}>
                    <img alt="Food Ordering" src={require("../../../assests/images/DineIn.jpg")}
                         className={"img-dinein"}/>
                </Col>
                <Col className={"dine-in-right-col"} xs={12} md={6} lg={6}>
                    <h1 className="intro-heading">Increase Dine In and <br/>Takeout Orders</h1>
                    <p className="intro-text-subtitle">Customer will love to order frequently from your
                        restaurant as they get sweet rewards and also save tons of time.</p>
                </Col>
            </Row>
        </div>
    );
}
export default DineInService
  