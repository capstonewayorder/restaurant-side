//Author : Vishvakumar Mavani
import React from "react";
import Lottie from "react-lottie";
import "./Error404.css"
import error404 from "./assests/images/error404.json";
import RestaurantPageNavBar from "./RestaurantSite/NavBar/RestaurantPageNavBar";

const Error404 = props => {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: error404,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return (
        <div className={"container"} style={{alignItems: 'center'}}>
            <RestaurantPageNavBar displayLogin={false}/>
            <Lottie options={defaultOptions} style={{width: '60%', paddingTop: '8%'}}/>
        </div>
    )
}
export default Error404