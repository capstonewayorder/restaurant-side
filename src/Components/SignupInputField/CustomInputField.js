//Author : Rohan Patel
import React from "react";
import "./CustomInputFieldStyle.css"

const CustomInputField = props => {
    return (
        <input type={props.type}
               className={"custom-input-field form-control"}
               placeholder={props.placeholder}
               onChange={event => props.onChange(event.target.value)}
               style={props.error ? {border: '1.7px solid #FF5050'} : {}}
               onKeyDown={props.onEnterKeyPress}
        />

    )
};

export default CustomInputField