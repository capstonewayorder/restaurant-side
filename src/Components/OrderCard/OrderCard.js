//Author : Rohan Patel

import React from 'react';
import {Row, Col, Table} from "reactstrap";

const OrderCard = props => {
    const order = props.order;
    return (
        <div>
            <Row className="shadow p-3 mt-3 mr-2 ml-2 bg-white rounded orderItemCard">
                <Col><span className="text-monospace text-muted">#{order.id}</span></Col>
                <Col>{order.user.fullname}</Col>
                <Col>{order.orderPlacedTime}</Col>
                <Col>{order.orderItems.length}</Col>
            </Row>
        </div>
    );
}

export default OrderCard;